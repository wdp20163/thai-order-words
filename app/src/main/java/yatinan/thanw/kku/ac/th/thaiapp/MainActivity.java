package yatinan.thanw.kku.ac.th.thaiapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton game, learn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        game = (ImageButton) findViewById(R.id.ib_Game);
        learn = (ImageButton) findViewById(R.id.ib_Book);

        game.setOnClickListener(this);
        learn.setOnClickListener(this);

        startService(new Intent(this, BackgroundSoundService.class));
    }

    @Override
    public void onClick(View v) {
        if (v == game) {
            Intent intentGame = new Intent(MainActivity.this, GameActivity.class);
            startActivity(intentGame);

        }
        if (v == learn) {
            Intent intent = new Intent(MainActivity.this, Learning.class);
            startActivity(intent);

        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("คุณต้องการออกจากแอปพลิเคชั่น ?");
        alertDialogBuilder
                .setMessage("กด \"ใช่\" เพื่อออก")
                .setCancelable(false)
                .setPositiveButton("ใช่",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                stopService(new Intent(MainActivity.this, BackgroundSoundService.class));
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("ไม่ใช่", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!hasWindowFocus()) {
            stopService(new Intent(this, BackgroundSoundService.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, BackgroundSoundService.class));
    }
}

