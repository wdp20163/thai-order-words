package yatinan.thanw.kku.ac.th.thaiapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TotalScoreActivity extends AppCompatActivity implements View.OnClickListener {
    TextView total_score;
    Button restart, home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_score);
        restart = (Button) findViewById(R.id.restart);
        restart.setOnClickListener(this);

        home = (Button) findViewById(R.id.home);
        home.setOnClickListener(this);

        Intent intent = getIntent();
        int score = intent.getIntExtra("score", 0);
        total_score = (TextView) findViewById(R.id.total_score);
        total_score.setText(String.valueOf(score));
    }

    @Override
    public void onClick(View v) {
        if (v == restart) {
            Intent intent = new Intent(TotalScoreActivity.this, GameActivity.class);
            startActivity(intent);
        }
        if (v == home) {
            Intent intent = new Intent(TotalScoreActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TotalScoreActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!hasWindowFocus()) {
            stopService(new Intent(this, BackgroundSoundService.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, BackgroundSoundService.class));
    }
}
