package yatinan.thanw.kku.ac.th.thaiapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvScore;
    ImageView tvQuestion, tvHeart1, tvHeart2, tvHeart3;
    ImageButton chLeft, chRight, picLeft, picRight;
    Button skip_btn;
    MediaPlayer correctSound, incorrectSound;
    final Random rnd = new Random();
    final int MIN_QUESTION = 1, MAX_QUESTION = 2;
    final int MIN_ANSWER = 1, MAX_ANSWER = 44;
    final int sdk = android.os.Build.VERSION.SDK_INT;
    int rnd_numLeft, rnd_numRight, rnd_numQuestion, check, correct;
    int score = 0;
    int heart = 3;
    int mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Log.d("Check Activity", this.getClass().getName().toString());

        tvQuestion = (ImageView) findViewById(R.id.question);
        tvScore = (TextView) findViewById(R.id.score);

        tvHeart1 = (ImageView) findViewById(R.id.heart1);
        tvHeart2 = (ImageView) findViewById(R.id.heart2);
        tvHeart3 = (ImageView) findViewById(R.id.heart3);

        chLeft = (ImageButton) findViewById(R.id.ch1);
        chLeft.setOnClickListener(this);
        chRight = (ImageButton) findViewById(R.id.ch2);
        chRight.setOnClickListener(this);

        picLeft = (ImageButton) findViewById(R.id.pic1);
        picLeft.setOnClickListener(this);
        picRight = (ImageButton) findViewById(R.id.pic2);
        picRight.setOnClickListener(this);

        skip_btn = (Button) findViewById(R.id.skip_btn);
        skip_btn.setOnClickListener(this);

        correctSound = MediaPlayer.create(GameActivity.this, R.raw.correct);
        incorrectSound = MediaPlayer.create(GameActivity.this, R.raw.incorrect);

        final Animation animationLeft = new AlphaAnimation((float) 0.9, 0); // Change alpha from fully visible to invisible
        animationLeft.setDuration(500); // duration - half a second
        animationLeft.setInterpolator(new LinearInterpolator()); // do not alter
        // animation
        // rate
        animationLeft.setRepeatCount(Animation.INFINITE); // Repeat animation
        // infinitely
        animationLeft.setRepeatMode(Animation.REVERSE); // Reverse animation at the
        // end so the button will
        // fade back in

        picLeft.startAnimation(animationLeft);

        final Animation animationRight = new AlphaAnimation(0, (float) 0.9); // Change alpha from fully visible to invisible
        animationRight.setDuration(500); // duration - half a second
        animationRight.setInterpolator(new LinearInterpolator()); // do not alter
        // animation
        // rate
        animationRight.setRepeatCount(Animation.INFINITE); // Repeat animation
        // infinitely
        animationRight.setRepeatMode(Animation.REVERSE); // Reverse animation at the
        // end so the button will
        // fade back in
        picRight.startAnimation(animationRight);


        //Remove next button
        //next_btn = (Button) findViewById(R.id.next_btn);
        //next_btn.setOnClickListener(this);

        randomQuestion();
        randomAnswer();
    }

    protected void randomQuestion() {
        rnd_numQuestion = rnd.nextInt((MAX_QUESTION - MIN_QUESTION) + 1) + MIN_QUESTION;
        final String strQuestion = "question_" + rnd_numQuestion;
        tvQuestion.setImageDrawable
                (
                        getResources().getDrawable(getResourceID(strQuestion, "drawable",
                                getApplicationContext()))
                );
    }

    protected void randomAnswer() {
        do {
            rnd_numLeft = rnd.nextInt((MAX_ANSWER - MIN_ANSWER) + 1) + MIN_ANSWER;
            rnd_numRight = rnd.nextInt((MAX_ANSWER - MIN_ANSWER) + 1) + MIN_ANSWER;
        } while (rnd_numLeft == rnd_numRight);

        final String strLeft = "ch_" + rnd_numLeft;
        final String str_picLeft = "pic_" + rnd_numLeft;

        final String strRight = "ch_" + rnd_numRight;
        final String str_picRight = "pic_" + rnd_numRight;
        chLeft.setImageDrawable
                (
                        getResources().getDrawable(getResourceID(strLeft, "drawable",
                                getApplicationContext()))
                );

        picLeft.setImageDrawable
                (
                        getResources().getDrawable(getResourceID(str_picLeft, "drawable",
                                getApplicationContext()))
                );

        chRight.setImageDrawable
                (
                        getResources().getDrawable(getResourceID(strRight, "drawable",
                                getApplicationContext()))
                );

        picRight.setImageDrawable
                (
                        getResources().getDrawable(getResourceID(str_picRight, "drawable",
                                getApplicationContext()))
                );
    }

    protected final static int getResourceID
            (final String resName, final String resType, final Context ctx) {
        final int ResourceID =
                ctx.getResources().getIdentifier(resName, resType,
                        ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException
                    (
                            "No resource string found with name " + resName
                    );
        } else {
            return ResourceID;
        }
    }

    protected void checkAnswer(int checkAns) {
        mode = 1;
        if (rnd_numQuestion == 1) {
            if (rnd_numLeft < rnd_numRight) {
                if (checkAns == 1) {
                    correct = 1;
                    chLeft.setBackgroundColor(Color.parseColor("#87EA00"));
                    //picLeft.setBackgroundColor(Color.GREEN);
                    correctSound.start();
                } else {
                    correct = 0;
                    chRight.setBackgroundColor(Color.parseColor("#FF0700"));
                    //picRight.setBackgroundColor(Color.RED);
                    incorrectSound.start();
                }
            } else if (rnd_numRight < rnd_numLeft) {
                if (checkAns == 2) {
                    correct = 1;
                    chRight.setBackgroundColor(Color.parseColor("#87EA00"));
                    //picRight.setBackgroundColor(Color.GREEN);
                    correctSound.start();
                } else {
                    correct = 0;
                    chLeft.setBackgroundColor(Color.parseColor("#FF0700"));
                    //picLeft.setBackgroundColor(Color.RED);
                    incorrectSound.start();
                }
            }
        } else if (rnd_numQuestion == 2) {
            if (rnd_numLeft > rnd_numRight) {
                if (checkAns == 1) {
                    correct = 1;
                    chLeft.setBackgroundColor(Color.parseColor("#87EA00"));
                    //picLeft.setBackgroundColor(Color.GREEN);
                    correctSound.start();
                } else {
                    correct = 0;
                    chRight.setBackgroundColor(Color.parseColor("#FF0700"));
                    //picRight.setBackgroundColor(Color.RED);
                    incorrectSound.start();
                }
            } else if (rnd_numRight > rnd_numLeft) {
                if (checkAns == 2) {
                    correct = 1;
                    chRight.setBackgroundColor(Color.parseColor("#87EA00"));
                    //picRight.setBackgroundColor(Color.GREEN);
                    correctSound.start();
                } else {
                    correct = 0;
                    chLeft.setBackgroundColor(Color.parseColor("#FF0700"));
                    //picLeft.setBackgroundColor(Color.RED);
                    incorrectSound.start();
                }
            }
        }

    }

    protected void setHeart(int heart) {
        switch (heart) {
            case 0:
                tvHeart1.setVisibility(View.INVISIBLE);
                tvHeart2.setVisibility(View.INVISIBLE);
                tvHeart3.setVisibility(View.INVISIBLE);
                break;
            case 1:
                tvHeart1.setVisibility(View.VISIBLE);
                tvHeart2.setVisibility(View.INVISIBLE);
                tvHeart3.setVisibility(View.INVISIBLE);
                break;
            case 2:
                tvHeart1.setVisibility(View.VISIBLE);
                tvHeart2.setVisibility(View.VISIBLE);
                tvHeart3.setVisibility(View.INVISIBLE);
                break;
            case 3:
                tvHeart1.setVisibility(View.VISIBLE);
                tvHeart2.setVisibility(View.VISIBLE);
                tvHeart3.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        if (v == chLeft || v == picLeft) {
            if (mode == 0) {
                check = 1;
                checkAnswer(check);
                delay();
            }
        }
        if (v == chRight || v == picRight) {
            if (mode == 0) {
                check = 2;
                checkAnswer(check);
                delay();
            }
        }
//        if (v == next_btn) {
//            if (mode == 1) {
//                if (correct == 1) {
//                    score = score + 1;
//                } else if (correct == 0) {
//                    heart = heart - 1;
//                }
//                tvScore.setText("คะแนน: " + score);
//                setHeart(heart);
//                if (heart != 0) {
//                    randomQuestion();
//                    randomAnswer();
//                } else {
//                    Intent intent = new Intent(GameActivity.this, TotalScoreActivity.class);
//                    intent.putExtra("score", score);
//                    startActivity(intent);
//                }
//            }
//            chLeft.setBackgroundColor(Color.TRANSPARENT);
//            picLeft.setBackgroundColor(Color.TRANSPARENT);
//            chRight.setBackgroundColor(Color.TRANSPARENT);
//            picRight.setBackgroundColor(Color.TRANSPARENT);
//            mode = 0;
//            correct = 0;
//            delay();
//        }

        if (v == skip_btn) {
            if (mode == 0) {
                if (score > 0) {
                    score = score - 1;
                    randomQuestion();
                    randomAnswer();
                } else {
                    score = 0;
                }
                tvScore.setText("คะแนน: " + score);
                setHeart(heart);
            }
        }

    }

    public void nextQuestion() {

        if (mode == 1) {
            if (correct == 1) {
                score = score + 1;
            } else if (correct == 0) {
                heart = heart - 1;
            }
            tvScore.setText("คะแนน: " + score);
            setHeart(heart);
            if (heart != 0) {
                randomQuestion();
                randomAnswer();
            } else {
                Intent intent = new Intent(GameActivity.this, TotalScoreActivity.class);
                intent.putExtra("score", score);
                startActivity(intent);
            }
        }

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            chLeft.setBackgroundDrawable(getResources().getDrawable(R.drawable.ch_btn_background));
            chRight.setBackgroundDrawable(getResources().getDrawable(R.drawable.ch_btn_background));
        } else {
            chLeft.setBackground(getResources().getDrawable(R.drawable.ch_btn_background));
            chRight.setBackground(getResources().getDrawable(R.drawable.ch_btn_background));
        }

        picLeft.setBackgroundColor(Color.TRANSPARENT);
        picRight.setBackgroundColor(Color.TRANSPARENT);
        mode = 0;
        correct = 0;
    }

    public void delay() {
        new CountDownTimer(1500, 1000) {
            public void onFinish() {
                // When timer is finished
                // Execute your code here
                nextQuestion();
            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("คุณต้องการออกจากเกมส์ ?");
        alertDialogBuilder
                .setMessage("กด \"ใช่\" เพื่อออก")
                .setCancelable(false)
                .setPositiveButton("ใช่",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(GameActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        })

                .setNegativeButton("ไม่ใช่", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!hasWindowFocus()) {
            stopService(new Intent(this, BackgroundSoundService.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, BackgroundSoundService.class));
    }
}
